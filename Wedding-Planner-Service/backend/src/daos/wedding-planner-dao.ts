import { Expense, Wedding } from "../entities";

export interface WeddingDAO{

    createWedding(wedding:Wedding):Promise<Wedding>;

    getAllWeddings():Promise<Wedding[]>;

    getWeddingById(weddingId:number):Promise<Wedding>;

    updateWedding(wedding:Wedding):Promise<Wedding>;

    deleteWeddingById(weddingId:number):Promise<boolean>;

    createExpense(expense:Expense, weddingId:number):Promise<Expense>;

    getAllExpenses():Promise<Expense[]>;

    getExpenseById(expenseId:number):Promise<Expense>;

    getAllExpensesOfWedding(weddingId:number):Promise<Expense[]>;

    updateExpense(expense:Expense):Promise<Expense>;

    deleteExpense(expenseId:number):Promise<boolean>;
}