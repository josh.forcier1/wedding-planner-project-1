import { client } from "../connection";
import { Wedding, Expense } from "../entities";
import { MissingResourceError } from "../errors";
import { WeddingDAO } from "./wedding-planner-dao";

export class WeddingPlannerDaoPostgress implements WeddingDAO{
    async createWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = 'insert into wedding (wedding_date,wedding_location,wedding_name,budget) values ($1,$2,$3,$4) returning wedding_id';
        const values = [wedding.date, wedding.location, wedding.name, wedding.budget];
        const result = await client.query(sql,values);
        wedding.weddingId = result.rows[0].wedding_id;
        return wedding;
    }

    async getAllWeddings(): Promise<Wedding[]> {
        const sql:string = 'select * from wedding';
        const result = await client.query(sql);
        const weddings:Wedding[] = [];
        for (const row of result.rows){
            const wedding:Wedding = new Wedding(row.wedding_id, row.wedding_date, row.wedding_location, row.wedding_name, row.budget);
            weddings.push(wedding);
        }
        return weddings;
    }

    async getWeddingById(weddingId: number): Promise<Wedding> {
        const sql:string = 'select * from wedding where wedding_id = $1';
        const values = [weddingId];
        const result = await client.query(sql,values);
        if (result.rowCount === 0){
            throw new MissingResourceError(`The wedding with id ${weddingId} does not exist`);
        }
        const row = result.rows[0];
        const wedding:Wedding = new Wedding(row.wedding_id, row.wedding_date, row.wedding_location, row.wedding_name, row.budget);
        return wedding;

    }

    async updateWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = 'update wedding set wedding_date = $1, wedding_location = $2, wedding_name = $3, budget = $4 where wedding_id = $5';
        const values = [wedding.date, wedding.location, wedding.name, wedding.budget, wedding.weddingId];
        const result = await client.query(sql,values);
        if (result.rowCount ===0){
            throw new MissingResourceError(`The wedding with id ${wedding.weddingId} does not exist`);
        }
        return wedding;

    }
    async deleteWeddingById(weddingId: number): Promise<boolean> {
        const sql:string = 'delete from wedding where wedding_id = $1';
        const values = [weddingId];
        const result = await client.query(sql,values);
        if (result.rowCount === 0){
            throw new MissingResourceError(`The client wedding id ${weddingId} does not exist`);
        }
        return true;
    }

    async createExpense(expense: Expense, weddingId:number): Promise<Expense> {
        try {
            const sql = 'insert into expense (reason, amount, w_id) values($1,$2,$3) returning expense_id, w_id';
            const values = [expense.reason,expense.amount,weddingId];
            const result = await client.query(sql,values);
            expense.expenseId = result.rows[0].expense_id;
            expense.foreignWeddingId = result.rows[0].w_id;
            return expense;
        } 
        catch (error) {
            throw new MissingResourceError(`Cannot create expense, the wedding with id ${weddingId} does not exist`)   
        }
    }

    async getAllExpenses(): Promise<Expense[]> {
        const sql:string = 'select * from expense';
        const result = await client.query(sql);
        const expenses:Expense[] = [];
        for (const row of result.rows){
            const expense:Expense = new Expense(row.expense_id,row.reason,row.amount,row.w_id);
            expenses.push(expense);
        }
        if (result.rowCount === 0){
            throw new MissingResourceError(`There are no expenses`);
        }
        return expenses;
    }

    async getExpenseById(expenseId: number): Promise<Expense> {
        const sql:string = 'select * from expense where expense_id = $1';
        const values = [expenseId];
        const result = await client.query(sql,values);
        const row = result.rows[0];
        if (result.rowCount === 0){
            throw new MissingResourceError(`The expense with id ${expenseId} does not exist`);
        }
        const expense:Expense = new Expense(row.expense_id,row.reason,row.amount,row.w_id);
        return expense;
    }
    async getAllExpensesOfWedding(weddingId: number): Promise<Expense[]> {
        const sql:string = 'select * from expense where w_id = $1';
        const values = [weddingId];
        const result = await client.query(sql,values);
        const expenses:Expense[] = [];
        for (const row of result.rows){
            const expense:Expense = new Expense(row.expense_id,row.reason,row.amount,row.w_id);
            expenses.push(expense);
        }
        if (result.rowCount === 0){
            throw new MissingResourceError(`Either the wedding with id ${weddingId} does not exist or they have no expenses`);
        }
        return expenses;
    }

    async updateExpense(expense: Expense): Promise<Expense> {
        const sql:string = 'update expense set reason = $1, amount = $2 where expense_id = $3';
        const values = [expense.reason,expense.amount,expense.expenseId];
        const result = await client.query(sql,values);
        if (result.rowCount ===0){
            throw new MissingResourceError(`The expense with id ${expense.expenseId} does not exist`);
        }
        return expense;
    }

    async deleteExpense(expenseId: number): Promise<boolean> {
        const sql:string = 'delete from expense where expense_id = $1';
        const values = [expenseId];
        const result= await client.query(sql,values);
        if (result.rowCount ===0){
            throw new MissingResourceError(`The expense with id ${expenseId} does not exist`);
        }
        return true;
    }
    
}
