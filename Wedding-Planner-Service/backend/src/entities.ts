
export class Wedding{
    constructor(
        public weddingId:number,
        public date:string,
        public location:string,
        public name:string,
        public budget:number,
    ){}
}

export class Expense{
    constructor(
        public expenseId:number,
        public reason:string,
        public amount:number,
        public foreignWeddingId:number
    ){}
}