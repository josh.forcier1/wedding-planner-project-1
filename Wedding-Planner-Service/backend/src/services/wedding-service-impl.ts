import { WeddingDAO } from "../daos/wedding-planner-dao";
import { WeddingPlannerDaoPostgress } from "../daos/wedding-planner-dao-postgres";
import { Wedding, Expense } from "../entities";
import WeddingService from "./wedding-service";

export class WeddingServiceImpl implements WeddingService{

    weddingDAO:WeddingDAO = new WeddingPlannerDaoPostgress();

    registerWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDAO.createWedding(wedding);
    }

    retrieveAllWeddings(): Promise<Wedding[]> {
        return this.weddingDAO.getAllWeddings();
    }

    retrieveWeddingById(weddingId: number): Promise<Wedding> {
        return this.weddingDAO.getWeddingById(weddingId);
    }

    updateWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDAO.updateWedding(wedding);
    }

    deleteWeddingById(weddingId: number): Promise<boolean> {
        return this.weddingDAO.deleteWeddingById(weddingId);
    }

    registerExpense(expense: Expense, weddingId: number): Promise<Expense> {
        return this.weddingDAO.createExpense(expense,weddingId);
    }

    retrieveAllExpenses(): Promise<Expense[]> {
        return this.weddingDAO.getAllExpenses();
    }

    retrieveExpenseById(expenseId: number): Promise<Expense> {
        return this.weddingDAO.getExpenseById(expenseId);
    }

    retrieveAllExpensesOfWedding(weddingId: number): Promise<Expense[]> {
        return this.weddingDAO.getAllExpensesOfWedding(weddingId);
    }

    updateExpense(expense: Expense): Promise<Expense> {
        return this.weddingDAO.updateExpense(expense);
    }

    deleteExpense(expenseId: number): Promise<boolean> {
        return this.weddingDAO.deleteExpense(expenseId);
    }
    
}