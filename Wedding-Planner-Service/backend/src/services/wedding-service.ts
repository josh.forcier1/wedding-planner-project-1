import { Expense, Wedding } from "../entities";

export default interface WeddingService{

    registerWedding(wedding:Wedding):Promise<Wedding>;

    retrieveAllWeddings():Promise<Wedding[]>;

    retrieveWeddingById(weddingId:number):Promise<Wedding>;

    updateWedding(wedding:Wedding):Promise<Wedding>;

    deleteWeddingById(weddingId:number):Promise<boolean>;

    registerExpense(expense:Expense, weddingId:number):Promise<Expense>;

    retrieveAllExpenses():Promise<Expense[]>;

    retrieveExpenseById(expenseId:number):Promise<Expense>;

    retrieveAllExpensesOfWedding(weddingId:number):Promise<Expense[]>;

    updateExpense(expense:Expense):Promise<Expense>;

    deleteExpense(expenseId:number):Promise<boolean>;
}