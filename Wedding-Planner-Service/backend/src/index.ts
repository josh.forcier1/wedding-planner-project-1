import express from "express";
import cors from 'cors'
import WeddingService from "./services/wedding-service";
import { WeddingServiceImpl } from "./services/wedding-service-impl";
import { Wedding, Expense } from "./entities";
import { MissingResourceError } from "./errors";

const app = express();
app.use(express.json());
app.use(cors());

const weddingService:WeddingService = new WeddingServiceImpl();

app.post("/weddings", async (req,res) =>{
    let wedding:Wedding = req.body;
    wedding = await weddingService.registerWedding(wedding);
    res.status(201);
    res.send(wedding)
}); 

app.get("/weddings", async(req,res) =>{
    try {
        const weddings:Wedding[]= await weddingService.retrieveAllWeddings()
        res.status(200);
        res.send(weddings);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.get("/weddings/:id", async(req,res) =>{
    try {
        const weddingId:number = Number(req.params.id);
        const wedding:Wedding = await weddingService.retrieveWeddingById(weddingId);
        res.send(wedding)
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.put("/weddings/:id", async(req,res) =>{
    try {
        let wedding:Wedding = req.body;
        const weddingId:number = Number(req.params.id);
        wedding.weddingId = weddingId;
        wedding = await weddingService.updateWedding(wedding);
        res.send(wedding);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

app.delete("/weddings/:id", async(req,res)=>{
    try {
        const weddingId:number = Number(req.params.id);
        const bool:boolean = await weddingService.deleteWeddingById(weddingId);
        res.status(205);
        res.send(bool);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

app.post("/weddings/:id/expenses", async (req,res) =>{
    try {
        let expense = req.body;
        const weddingId:number = Number(req.params.id);
        expense = await weddingService.registerExpense(expense,weddingId);
        res.status(201);
        res.send(expense)
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
}); 

app.get("/expenses", async (req,res) =>{
    try {
        const expenses:Expense[]= await weddingService.retrieveAllExpenses()
        res.status(200);
        res.send(expenses);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

app.get("/weddings/:id/expenses", async (req,res) =>{
    try {
        const weddingId:number = Number(req.params.id);
        const expenses:Expense[]= await weddingService.retrieveAllExpensesOfWedding(weddingId)
        res.status(200);
        res.send(expenses);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

app.get("/expenses/:id", async (req,res) =>{
    try {
        const expenseId:number = Number(req.params.id);
        const expense:Expense= await weddingService.retrieveExpenseById(expenseId)
        res.send(expense);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})


app.put("/expenses/:id", async(req,res) =>{
    try {
        let expense:Expense = req.body;
        const expenseId:number = Number(req.params.id);
        expense.expenseId = expenseId
        expense = await weddingService.updateExpense(expense)
        res.send(expense);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

app.delete("/expenses/:id", async(req,res)=>{
    try {
        const expenseId:number = Number(req.params.id);
        const bool:boolean = await weddingService.deleteExpense(expenseId);
        res.status(205);
        res.send(bool);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

app.listen(5432,()=>{console.log("Application Started")});