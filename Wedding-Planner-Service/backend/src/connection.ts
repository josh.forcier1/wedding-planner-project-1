import {Client} from 'pg';

export const client = new Client({
    user:'postgres',
    password:process.env.DBPASSWORD,
    database:'plannerdb',
    port:5432,
    host:'35.202.197.203'
})
client.connect();