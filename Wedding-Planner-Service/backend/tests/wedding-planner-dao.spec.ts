import { WeddingPlannerDaoPostgress } from "../src/daos/wedding-planner-dao-postgres"
import { WeddingDAO } from "../src/daos/wedding-planner-dao"
import { Expense, Wedding } from "../src/entities"
import { client } from "../src/connection";

const weddingDao:WeddingDAO = new WeddingPlannerDaoPostgress;

test("Create a wedding", async ()=>{
    const testWedding:Wedding = new Wedding(0,'9/1/2021','Albany NY', 'My Wedding',10000);
    const result:Wedding = await weddingDao.createWedding(testWedding);
    expect(result.weddingId).not.toBe(0);
})

test("Get all weddings", async ()=>{
    let testWedding1:Wedding = new Wedding(0,'9/1/2021','Albany NY', 'My Wedding',10000);
    testWedding1 = await weddingDao.createWedding(testWedding1)
    let testWedding2:Wedding = new Wedding(0,'10/5/2022','Vermont', 'Best We',15000);
    testWedding2 = await weddingDao.createWedding(testWedding2)
    let testWedding3:Wedding = new Wedding(0,'7/16/2023','Morgantown WV', 'Tropic theme',23000);         
    testWedding3 = await weddingDao.createWedding(testWedding3)          
    
    const weddings:Wedding[] = await weddingDao.getAllWeddings();
    expect(weddings.length).toBeGreaterThanOrEqual(3);
})

test("Get wedding by Id", async () =>{
    let testWedding:Wedding = new Wedding(0,'9/1/2021','Albany NY', 'My Wedding',10000);
    testWedding = await weddingDao.createWedding(testWedding)
    let retrievedWedding:Wedding = await weddingDao.getWeddingById(testWedding.weddingId);
    expect(retrievedWedding.name).toBe(testWedding.name);
});

test("Update wedding", async () =>{
    let testWedding:Wedding = new Wedding(0,'9/1/2021','Albany NY', 'My Wedding',10000);
    testWedding = await weddingDao.createWedding(testWedding);
    testWedding.location = 'New York NY';
    testWedding = await weddingDao.updateWedding(testWedding);
    expect(testWedding.location).toBe('New York NY');
});

test("Delete wedding by id", async () => {
    let testWedding:Wedding = new Wedding(0,'9/1/2021','Albany NY', 'My Wedding',10000);
    testWedding = await weddingDao.createWedding(testWedding);
    const result:boolean = await weddingDao.deleteWeddingById(testWedding.weddingId);
    expect(result).toBeTruthy();
})

test("Create an expense",async ()=>{
    let testWedding:Wedding = new Wedding(0,'9/15/2025','Plattsburgh NY', 'Wedding to end all weddings',10000);
    testWedding = await weddingDao.createWedding(testWedding);
    let expense:Expense = new Expense(0, 'Wedding Cake', 500, 0);
    const result:Expense = await weddingDao.createExpense(expense, testWedding.weddingId);
    expect(result.expenseId).not.toBe(0);
});

test("Get all expenses", async ()=>{
    let testWedding:Wedding = new Wedding(0,'9/15/2025','Plattsburgh NY', 'Wedding to end all weddings',10000);
    testWedding = await weddingDao.createWedding(testWedding);
    let expense1:Expense = new Expense(0, 'Wedding Cake', 500, 0);
    expense1 = await weddingDao.createExpense(expense1, testWedding.weddingId);
    let expense2:Expense = new Expense(0, 'Wedding Cake', 500, 0);
    expense2 = await weddingDao.createExpense(expense2, testWedding.weddingId);
    let expense3:Expense = new Expense(0, 'Wedding Cake', 500, 0);
    expense3 = await weddingDao.createExpense(expense3, testWedding.weddingId);

    
    const expenses:Expense[] = await weddingDao.getAllExpenses();
    expect(expenses.length).toBeGreaterThanOrEqual(3);
});

test("Get expense by Id", async () =>{
    let testWedding:Wedding = new Wedding(0,'9/15/2025','Plattsburgh NY', 'Wedding to end all weddings',10000);
    testWedding = await weddingDao.createWedding(testWedding);
    let expense:Expense = new Expense(0, 'Wedding Cake', 500, 0);
    expense = await weddingDao.createExpense(expense, testWedding.weddingId);
    let retrievedExpense:Expense = await weddingDao.getExpenseById(expense.expenseId);
    expect(retrievedExpense.reason).toBe(expense.reason);
});

test("Get all expenses of a wedding", async ()=>{
    let testWedding:Wedding = new Wedding(0,'9/15/2025','Plattsburgh NY', 'Wedding to end all weddings',10000);
    testWedding = await weddingDao.createWedding(testWedding);
    let expense1:Expense = new Expense(0, 'Wedding Cake', 500, 0);
    expense1 = await weddingDao.createExpense(expense1, testWedding.weddingId);
    let expense2:Expense = new Expense(0, 'Wedding Cake', 500, 0);
    expense2 = await weddingDao.createExpense(expense2, testWedding.weddingId);
    let expense3:Expense = new Expense(0, 'Wedding Cake', 500, 0);
    expense3 = await weddingDao.createExpense(expense3, testWedding.weddingId);    
    const expenses:Expense[] = await weddingDao.getAllExpensesOfWedding(testWedding.weddingId);
    expect(expenses.length).toEqual(3);
})

test("Update expense", async () =>{
    let testWedding:Wedding = new Wedding(0,'9/15/2025','Plattsburgh NY', 'Wedding to end all weddings',10000);
    testWedding = await weddingDao.createWedding(testWedding);
    let expense:Expense = new Expense(0, 'Wedding Cake', 500, 0);
    expense = await weddingDao.createExpense(expense, testWedding.weddingId);

    expense.amount = 1500;
    expense = await weddingDao.updateExpense(expense);
    expect(expense.amount).toBe(1500);
});

test("Delete expense by id", async () => {
    let testWedding:Wedding = new Wedding(0,'9/15/2025','Plattsburgh NY', 'Wedding to end all weddings',10000);
    testWedding = await weddingDao.createWedding(testWedding);
    let expense:Expense = new Expense(0, 'Wedding Cake', 500, 0);
    expense = await weddingDao.createExpense(expense, testWedding.weddingId);

    const result:boolean = await weddingDao.deleteExpense(expense.expenseId);
    expect(result).toBeTruthy();
})


afterAll(async() =>{
    client.end();
})