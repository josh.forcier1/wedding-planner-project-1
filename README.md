# Wedding Planner Project 1

## Project Description

The Wedding Planner app is an online planner for a small wedding planner company. Employees to create and manage weddings. Employees can also send each other messages. It is a micro-services cloud deployed application. The application makes use of many GCP services including, Google Compute Engine, Datastore, Cloud SQL, Datastore, App Engine, Cloud Functions, Cloud Storage and Firebase Hosting.

## Technologies Used

* PostgreSQL
* Nodejs
* Express
* Firebase
* Cloud SQL
* Compute Engine
* App Engine
* Datastore
* Cloud Storage
* Cloud Function

## Features

List of features ready
* Create and view weddings
* "Login" and set username in local storage
* Send and Receive messages
* Upload photo to Cloud Storage

## Getting Started
   
Link to clone repository: https://gitlab.com/josh.forcier1/wedding-planner-project-1.git

In order to run locally, run npm install within all the directories that have a package.json. Each service has a start script, run npm start via terminal or command line in order to start the services.

