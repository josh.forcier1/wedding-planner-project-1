const {Datastore} = require('@google-cloud/datastore')
const express = require('express')
const cors = require('cors');
const e = require('express');

const app = express();
app.use(express.json())
app.use(cors());
 
const datastore = new Datastore();

app.get('/messages', async(req,res) =>{
    const recipient = req.query.recipient;
    const sender = req.query.sender;
    if (recipient && sender){
        const query = datastore.createQuery("message").filter("recipient",'=', recipient).filter("sender",'=', sender);
        const [data] = await datastore.runQuery(query);
        if (data[0]){
            res.send(data)
        }
        else{
            res.status(404);
            res.send("No messages with that recipient and sender");
        }
    }
    else if (recipient){
        const query = datastore.createQuery("message").filter("recipient",'=', recipient);
        const [data] = await datastore.runQuery(query);
        if (data[0]){
            res.send(data)
        }
        else{
            res.status(404);
            res.send("No messages with that recipient");
        }
    }
    else if (sender){
        const query = datastore.createQuery("message").filter("sender",'=', sender);
        const [data] = await datastore.runQuery(query);
        if (data[0]){
            res.send(data)
        }
        else{
            res.status(404);
            res.send("No messages with that sender");
        }
    }
    else{
        const query = datastore.createQuery("message")
        const [data] = await datastore.runQuery(query);
        res.send(data)
    };
})

app.get('/messages/:mid', async (req,res) =>{
    const key = datastore.key(['message',Number(req.params.mid)]);
    const query = await datastore.createQuery('message').filter('__key__', '=', key);
    const [data] = await datastore.runQuery(query);
    if (data[0]){
        res.send(data)
    }
    else{
        res.status(404);
        res.send("Message id not found")
    }
 
})

app.post('/messages', async (req,res) =>{
    const message = req.body;
    const key = datastore.key(['message']);
    const response = await datastore.save({key:key,data:message});
    res.status(201);
    res.send('Successfully sent a new message');
})


const PORT = process.env.PORT || 3002;
app.listen(PORT,()=>console.log('Application started'));