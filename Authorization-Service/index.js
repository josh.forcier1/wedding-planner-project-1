const {Datastore} = require('@google-cloud/datastore')
const express = require('express')
const cors = require('cors')

const app = express();
app.use(express.json())
app.use(cors());

const datastore = new Datastore();

app.get('/users/:email/verify', async (req,res) =>{
    const email = req.params.email;
    const query = datastore.createQuery("employee").filter("email", "=", email);
    const [data] = await datastore.runQuery(query);
    if (data[0]){
        res.status(200);
        res.send(data);
    }
    else{
        res.status(404);
        res.send("Incorrect Email")
    }
})

app.patch('/users/login', async (req,res) =>{
    const body = req.body;
    const query = datastore.createQuery("employee").filter("email", "=", body.email).filter("password", "=", body.password);
    const [data] = await datastore.runQuery(query);
    if (data[0]){
        res.send(`{"fname":"${data[0].fname}", "lname":"${data[0].lname}"}`)
    }
    else{
        res.status(404);
        res.send("Incorrect login")
    }


})

const PORT = process.env.PORT || 3001;

app.listen(PORT,()=>console.log('Application started'));